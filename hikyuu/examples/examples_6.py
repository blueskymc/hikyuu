from hikyuu.interactive import *
import matplotlib.pyplot as plt

#创建一个初始资金10万元，起始日期2017年1月1日的模拟账户
my_tm = crtTM(init_cash=100000, date=Datetime(201701010000))

#2017年1月3日以9.11的价格买入100股
td = my_tm.buy(Datetime(201701030000), sm['sz000001'], 9.11, 100)

#查看当前资金及持仓情况
print(my_tm)

#转化为pandas的DataFrame显示当前持仓情况
position = my_tm.get_position_list()
position.to_df()

#2017年2月21日以9.60的价格卖出100股
td = my_tm.sell(Datetime(201702210000), sm['sz000001'], 9.60)

print(my_tm)

#在 hikyuu_XXX.ini 文件中配置的临时路径中输出
# my_tm.tocsv(sm.tmpdir())

# from datetime import date
# filename = "{}/my_trade/my_trade_record_{}.xml".format(sm.tmpdir(), date.today());
# hku_save(my_tm, filename)

#载入已保存的TM对象
#filename = "{}/my_trade/my_trade_record_{}.xml".format(sm.tmpdir(), date.today())
# new_my_tm = hku_load(filename)

#创建模拟交易账户进行回测，初始资金30万
my_tm = crtTM(init_cash=300000, date=Datetime(201701010000))

#注册实盘交易订单代理
ob = crtOB(TestOrderBroker())
my_tm.reg_broker(ob) #TestOerderBroker是测试用订单代理对象，只打印
# 注意：pybind 不支持下面这种方式调用，必须先生成实例再传入！！！
# my_tm.reg_broker(crtOB(TestOrderBroker(), False))

#根据需要修改订单代理最后的时间戳，后续只有大于该时间戳时，订单代理才会实际发出订单指令
my_tm.broker_last_datetime=Datetime(201701010000)

#创建信号指示器（以5日EMA为快线，5日EMA自身的10日EMA作为慢线，快线向上穿越慢线时买入，反之卖出）
my_sg = SG_Flex(EMA(C, n=5), slow_n=10)

#固定每次买入1000股
my_mm = MM_FixedCount(1000)

#创建交易系统并运行
sys = SYS_Simple(tm = my_tm, sg = my_sg, mm = my_mm)
sys.run(sm['sz000001'], Query(-150))