from hikyuu.interactive import *
import matplotlib.pyplot as plt

#创建一个从2001年1月1日开始的账户，初始资金20万元
my_tm = crtTM(Datetime(200101010000), 200000)

my_sys = SYS_Simple(tm=my_tm)


def TurtleSG(self, k):
    n1 = self.get_param("n1")
    n2 = self.get_param("n2")
    c = CLOSE(k)
    h = REF(HHV(c, n1), 1)  # 前n日高点
    L = REF(LLV(c, n2), 1)  # 前n日低点
    for i in range(h.discard, len(k)):
        if (c[i] >= h[i]):
            self._add_buy_signal(k[i].datetime)
        elif (c[i] <= L[i]):
            self._add_sell_signal(k[i].datetime)

my_sg = crtSG(TurtleSG, {'n1': 20, 'n2': 10}, 'TurtleSG')

my_mm = MM_Nothing()

s = sm['sz000001']
query = Query(Datetime(200101010000), Datetime(201705010000))

my_sys.mm = my_mm
my_sys.sg = my_sg
my_sys.run(s, query)

my_sys.performance()

per = Performance()
print(per.report(my_sys.tm, Datetime(datetime.today())))

calendar = sm.get_trading_calendar(query, 'SZ')
# calendar
x1 = my_tm.get_funds_curve(calendar, Query.DAY)
PRICELIST(x1).plot()

my_sys.mm = MM_FixedPercent(0.03)
my_sys.run(s, query)

x2 = my_tm.get_funds_curve(calendar, Query.DAY)
PRICELIST(x2).plot()

my_sys.mm = MM_FixedRisk(1000)
my_sys.run(s, query)

x3 = my_tm.get_funds_curve(calendar, Query.DAY)
PRICELIST(x3).plot()

my_sys.mm = MM_FixedCapital(1000)
my_sys.run(s, query)

x4 = my_tm.get_funds_curve(calendar, Query.DAY)
PRICELIST(x4).plot()

ax = create_figure(1)

def x_plot(x, name, ax):
    px = PRICELIST(x)
    px.name = name
    px.plot(axes=ax, legend_on=True)

x_plot(x1, 'MM_FixedCount', ax)
x_plot(x2, 'MM_FixedPercent', ax)
x_plot(x3, 'MM_FixedRisk', ax)
x_plot(x3, 'MM_FixedCapital', ax)

plt.show()