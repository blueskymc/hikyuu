from hikyuu.interactive import *
import matplotlib.pyplot as plt

s = sm['sh000001']
k = s.get_kdata(Query(-100))
k.plot()

#遍历查询最大收盘价
max_close = 0
for v in k:
    if v.close > max_close:
        max_close = v.close
print(max_close)

#查询股票前100个交易日的K线数据
k = s.get_kdata(Query(0, 100))

#查询股票最后100个交易日K线数据
k = s.get_kdata(Query(-100))

#查询股票第199个交易日到第209个交易日的K线数据
k = s.get_kdata(Query(200, 210))

#查询股票倒数第100个交易日至倒数第10个交易日的K线数据
k = s.get_kdata(Query(-100, -10))

#查询2017年1月1日至今的日线数据
k = s.get_kdata(Query(Datetime(201701010000)))

#查询2017年1月1日至3月31日日线数据
k = s.get_kdata(Query(Datetime(201701010000), Datetime(201704010000)))

#查询2017年1月5日1分钟线数据
k = s.get_kdata(Query(Datetime(201701050000), Datetime(201701060000), ktype=Query.MIN))
k.plot()

s = sm['sz000603']

#查询股票最后100个交易日K线数据，不复权
k = s.get_kdata(Query(-100))
k.plot()

#查询股票最后100个交易日K线数据，后向复权
k = s.get_kdata(Query(-100, recover_type=Query.BACKWARD))
k.plot()

plt.show()